/// This is the entry point for your add-on, creating and configuring
// your add-on HTTP server

/** [Express] {@link http://expressjs.com/}
 * Web framework that `atlassian-connect-express` uses
 * @module express/framework
 * @requires express
 */
/**
 * express module
 * @var
 */
var express = require('express');
var bodyParser = require('body-parser');
var compression = require('compression');
var cookieParser = require('cookie-parser');
var errorHandler = require('errorhandler');
var morgan = require('morgan');
// You need to load `atlassian-connect-express` to use her godly powers
var ac = require('atlassian-connect-express');
// Static expiry middleware to help serve static resources efficiently
process.env.PWD = process.env.PWD || process.cwd(); // Fix expiry on Windows :(
var expiry = require('static-expiry');

/** We use [Handlebars] {@link http://handlebarsjs.com/} as our view engine
 * via [express-hbs] {@link https://npmjs.org/package/express-hbs 
 * @module express/hbs
 * @requires express-hbs
 */
/**
 * express-hbs module
 * @var
 */
var hbs = require('express-hbs');
// We also need a few stock Node modules
var http = require('http');
var path = require('path');
var os = require('os');

// Anything in ./public is served up as static content
var staticDir = path.join(__dirname, 'public');
// Anything in ./views are HBS templates
var viewsDir = __dirname + '/views';
// Your routes live here; this is the C in MVC

/**
 * routes module
 * @var
 */
var routes = require('./routes');
// Bootstrap Express
/**
 * @description
 * Express router to mount handler functions and interact with Jira Service Desk Cloud instance, extending its UI.
 * Implements the Web UI modules, which can be attached to extension points to inject content into Jira Service Desk pages.
 * For detail information, please, visit the url: {@link  https://bitbucket.org/atlassian/atlassian-connect-express/src/master/}.
 * Also, it's highly related with the file "atlassian-connect.json" that is a manifest of all the extension points this module uses.
 * To see all of the available extension point options, check out the modules sections of the atlassian-connect documentation:{@link https://developer.atlassian.com/cloud/jira/service-desk/about-jira-modules/} and {@link https://developer.atlassian.com/cloud/jira/platform/about-jira-modules/}.
 * @type {object}
 * @var
 * @namespace app
 */
var app = express();
// Bootstrap the `atlassian-connect-express` library
// var addon = ac(app);

var addon = ac(app, {
    config: {
        descriptorTransformer: function (descriptor, config) {
            if (config.environment() === "production") {
                descriptor.key = "production-key";
            }
            return descriptor;
        }
    }
});
// You can set this in `config.json`
//var port = addon.config.port();
var port = process.env.PORT || 3000;
// Declares the environment to use in `config.json`
var devEnv = app.get('env') == 'development';

// The following settings applies to all environments
app.set('port', port);

// Configure the Handlebars view engine
app.engine('hbs', hbs.express3({ partialsDir: viewsDir }));
app.set('view engine', 'hbs');
app.set('views', viewsDir);

// Declare any Express [middleware](http://expressjs.com/api.html#middleware) you'd like to use here
// Log requests, using an appropriate formatter by env
app.use(morgan(devEnv ? 'dev' : 'combined'));
// Include request parsers
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
// Gzip responses when appropriate
app.use(compression());

// You need to instantiate the `atlassian-connect-express` middleware in order to get its goodness for free
app.use(addon.middleware());
// Enable static resource fingerprinting for far future expires caching in production
app.use(expiry(app, { dir: staticDir, debug: devEnv }));
// Add an hbs helper to fingerprint static resource urls
hbs.registerHelper('furl', function (url) { return app.locals.furl(url); });
// Mount the static resource dir
app.use(express.static(staticDir));

// Show nicer errors when in dev mode
if (devEnv) app.use(errorHandler());

// Wire up your routes using the express and `atlassian-connect-express` objects
routes(app, addon);

//// Log a Archivo
var fs = require('fs'); var util = require('util');
var log_file = fs.createWriteStream(__dirname + '/node.log', { flags: 'w' });
var log_stdout = process.stdout;

console.log = function (d) { //
    log_file.write(util.format(d) + '\n');
    log_stdout.write(util.format(d) + '\n');
};

////
// Boot the damn thing
http.createServer(app).listen(port, function () {
    console.log('Add-on server running at http://' + os.hostname() + ':' + port);
    // Enables auto registration/de-registration of add-ons into a host in dev mode
    if (devEnv) addon.register();
});
