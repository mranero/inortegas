var fs = require('fs');

 /**
 * File utilities module 
 * @module fileUtils
 * @example
 * var fileUtils = require('../utils_JC/fileUtils.js');
 */
module.exports = {
 /**
 * Write message into node.log archive.
 * @param {string} comentario - The message to write.
 * @example
 * fileUtils.comentar('/******** JIRA issue ==> ' + issuekey);
 */
	comentar: function(comentario){
		console.log('/*******************************************************************************************/');
	  	console.log('/**************************** '+ comentario +' *********************************************/');
	  	console.log(' DATE: ' + new Date());
	  	console.log('/*******************************************************************************************/');
	}

};