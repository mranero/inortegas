//The following will create an alert message every time the event customEvent is triggered.
AP.require('events', function (events) {
    events.on('customEvent', function () {
        var flag = AP.flag.create({
            title: 'Successfully created a flag.',
            body: 'This is a flag.',
            type: 'success',
            actions: {
                'actionkey': 'Click me'
            }
        });
    });
    events.emit('customEvent');
});