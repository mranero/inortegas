$(function (responseText) {
    var res = JSON.parse(responseText);
    var h;
    if (res.total > 0) {
        h = document.getElementById("idproduct");
        h.insertAdjacentHTML("afterend", "<form class='aui' id='f'><div class='field-group'><label for='select'><font face='Segoe UI'>Tu(s) equipo(s):</label><select class='select' id='idactive' name='idactive'></font></select></div></form>");
        var i;
        var x = "";
        for (i = 0; i < res.total; i++) {
            x += "<option value='" + res.issues[i].fields.summary + "'>" + res.issues[i].fields.issuetype.name + "-" + res.issues[i].fields.summary + "</option>";
        }
        h = document.getElementById("idactive");
        h.insertAdjacentHTML("afterbegin", x);
    } else {
        h = document.getElementById("idproduct");
        h.insertAdjacentHTML("afterend", "<blockquote><p>No se encuentra ningun equipo asociado" + response.values[0].reporter.emailAddress+"</p></blockquote>");
    }
    AP.require(["events"], function (events) {
        var requestProperties = new AP.jiraServiceDesk.RequestProperties();

        events.on("jira-servicedesk-request-properties-serialize", function () {
            requestProperties.serialize({
                iCMDB_active_issue_property: $("#idactive").val()
            });
        });

        events.on("jira-servicedesk-request-properties-validate", function () {
            var valid = true;
            //$("#f").append("<p class='field-error'>" + $("#priority").val()+"</p>");
            requestProperties.validate(valid)
        });
    });
});


