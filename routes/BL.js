var request = require('request');
var fs = require('fs');
module.exports = function () {
    var budget = {};

    return {
        storeBaseline(projectKey, bl, auth, urlJira) {
            //console.log("$$$$$$$$$$$" + JSON.stringify(bl));
            var options = {
                method: 'PUT',
                url: urlJira + '/rest/api/3/project/' + projectKey + '/properties/BL',
                headers: {
                    'Authorization': auth,
                    'Accept': 'application/json'
                },
                body: JSON.stringify(bl)
            };

            request(options, function (error, response, body) {
                if (error) throw new Error(error);
                console.log(
                    'Response: ' + response.statusCode + ' ' + response.statusMessage
                );
            });
        },
    }
};